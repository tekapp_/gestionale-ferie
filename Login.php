<html>
<head> <title> Login </title>
  <link rel="shortcut icon" href="C:\xampp\htdocs\Gestionale Ferie\graph-icon.ico">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="C:\xampp\htdocs\Gestionale Ferie\style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
  crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
  integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
  integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
  crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
  integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
  crossorigin="anonymous"></script>
</head>

<!-- -------------------------------------------------------------------------->

<style>
#login_button{
  margin-left:40%;
  padding: 20px 70px;
  background: #269900;
  color: white;
  margin-top: 5%;
  }

  #titolo {

    background: #269900;
    padding: 23px;
    color: white;
    margin-top: 5%;
  }

</style>

<!----------------------------------------------------------------------------->
  <body>
<!---div del titolo "Login" -->
    <div id="titolo">
         <h1 align="center"> Login </h1>
    </div>
<!-- div delle barre di inserimento e form in post al file db_connection.php-->
  <form action="db_connection.php" method="post">
  <div class="container" style="margin-top:3%">
    <div class="row">

      <div class="col-sm-12">
        <div class="form-group">
          <label for="inputdefault"> <strong>Inserisci il tuo nome utente</strong> </label>
          <input class="form-control" id="user_input" name="user_input" type="text"
           placeholder="Inserire nome utente">
          </div>



          <div class="form-group">
            <label for="pwd"><strong> Inserisci la tua password </strong> </label>
            <input type="password" class="form-control" name="pass_input"
            id="pass_input" placeholder="Inserire password">
          </div>

          <div class="form-group">
            <button type="submit" class="btn btn-lg active"
            id="login_button"><strong> Entra </strong ></button>
          </div>
        </div>
      </div>
    </div>
  </form>


 </body>
</html>
