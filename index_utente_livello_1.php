<html>
<head><title> Home (permessi di livello 1) </title>
  <link rel="shortcut icon" href="C:\xampp\htdocs\Gestionale Ferie\graph-icon.ico">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="C:\xampp\htdocs\Gestionale Ferie\style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
  crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
  integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
  integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
  crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
  integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
  crossorigin="anonymous"></script>
</head>
<style>

#head{
  background-color: #269900;
  color: white;
}

#giorno_toggle{
margin-left: 22%;
font-size: 26;

}


#ora_toggle{
  font-size: 26;
  margin-left: 22%;

}

#submit{
  background-color: #269900;
  font-style: strong;
  margin-left: 40%;
  padding: 20px 40px;
  position: absolute;
  color: white;
  top: 650px;
  left: 20px
}
</style>


<body>
<div clas="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="jumbotron" id="head">
      <h1> Prenota le tue ferie </h1>
        <h5> Richiedi qui le tue ferie </h5>
      </div>
    </div>
  </div>
</div>


<form action="richiesta_ferie.php" method="post">

<div class="container">
  <h2 style="margin-bottom: 5%">Seleziona giorno/i di ferie o le ore che si vuole prenotare(se disponibili)</h2>
  <ul class="nav nav-tabs">
    <li class="active" id="giorno_toggle"><a data-toggle="tab" href="#giorno"><strong>Giorno</strong></a></li>
    <li  id="ora_toggle"><a data-toggle="tab" href="#ora"><strong>Ora</strong></a></li>
</ul>

    <div class="tab-content">

        <div id="giorno" class="tab-pane fade in active">
        <h3 style="font-size: 58; color:#ff3333">Giorno</h3>
        <p style=""><strong>Seleziona data di inizio:</strong></p><br>
        <input type="date" id="giorno_inizio" name="date_start_input">
        <p style="margin-top: 5%"><strong>Seleziona data di fine: </strong></p>
        <input type="date" id="giorno_fine" name="date_finish_input">


    </div>
    <div id="ora" class="tab-pane fade">
      <h3 style="font-size: 58; color:ff3333">Ora </h3>
      <p><strong>Seleziona l'ora di inizio</strong></p>
      <input type="time" id="ora_inizio" name="start_time">
      <p style="margin-top: 5%;"><strong> Seleziona l'ora di fine</strong> </P>
        <input type="time" id="ora_fine" name="finish_time">
    </div>
  </div>
</div>

<input type="submit" class="btn btn-lg active" id="submit" value="Conferma Dati">

</form>


<!----------------------------------------------------------------------------->
<script>
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
});
</script>

  </body>
</html>
